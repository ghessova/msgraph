package com.contoso;

import com.google.gson.JsonPrimitive;
import com.microsoft.graph.logger.DefaultLogger;
import com.microsoft.graph.logger.LoggerLevel;
import com.microsoft.graph.models.extensions.*;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.microsoft.graph.requests.extensions.IEventCollectionPage;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ghessova on 17.03.2020.
 */
public class Graph {

    private static IGraphServiceClient graphClient = null;
    private static SimpleAuthProvider authProvider = null;

    private static void ensureGraphClient(String accessToken) {
        if (graphClient == null) {
            // Create the auth provider
            authProvider = new SimpleAuthProvider(accessToken);

            // Create default logger to only log errors
            DefaultLogger logger = new DefaultLogger();
            logger.setLoggingLevel(LoggerLevel.ERROR);

            // Build a Graph client
            graphClient = GraphServiceClient.builder()
                    .authenticationProvider(authProvider)
                    .logger(logger)
                    .buildClient();
        }
    }

    public static User getUser(String accessToken) {
        ensureGraphClient(accessToken);

        // GET /me to get authenticated user
        User me = graphClient
                .me()
                .buildRequest()
                .get();

        return me;
    }

    public static List<Event> getEvents(String accessToken) {
        ensureGraphClient(accessToken);

        // Use QueryOption to specify the $orderby query parameter
        final List<Option> options = new LinkedList<>();
        // Sort results by createdDateTime, get newest first
        options.add(new QueryOption("orderby", "createdDateTime DESC"));

        // GET /me/events
        IEventCollectionPage eventPage = graphClient
                .me()
                .events()
                .buildRequest(options)
                .select("subject,organizer,start,end")
                .get();

        return eventPage.getCurrentPage();
    }

    // https://docs.microsoft.com/en-us/graph/api/group-post-groups?view=graph-rest-1.0&tabs=java#request
    public static Group createGroup(String ownerId, String accessToken) {
        ensureGraphClient(accessToken);
        Group group = new Group();
        group.displayName = "My Group";
        group.description = "This is my test group.";
        LinkedList<String> groupTypesList = new LinkedList<String>();
        groupTypesList.add("Unified"); // Office 365
        group.groupTypes = groupTypesList;
        group.mailEnabled = true;
        group.mailNickname = "mygroup";
        group.securityEnabled = false;

        // owner
        // https://docs.microsoft.com/en-us/graph/api/group-post-groups?view=graph-rest-1.0&tabs=java#example-2-create-a-group-with-owners-and-members
        //group.additionalDataManager().put("owners@odata.bind", new JsonPrimitive("[https://graph.microsoft.com/v1.0/users/" + ownerId + "]")); // not working

        Group postResultGroup = graphClient.groups()
                .buildRequest()
                .post(group); // 403 Forbidden - permissions?

        return postResultGroup;

    }
}
